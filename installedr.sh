#!/bin/bash
echo -e "开始检查前置条件....."
corever=$(uname -a)
check80=$(sudo /bin/netstat -anlutp|awk '$4~/:80$/ {printf "%-6s %-25s %-25s %-s\n",$1,$4,$5,$6}')
check443=$(sudo /bin/netstat -anlutp|awk '$4~/:443$/ {printf "%-6s %-25s %-25s %-s\n",$1,$4,$5,$6}')
check8090=$(sudo /bin/netstat -anlutp|awk '$4~/:8090$/ {printf "%-6s %-25s %-25s %-s\n",$1,$4,$5,$6}')
check8004=$(sudo /bin/netstat -anlutp|awk '$4~/:8004$/ {printf "%-6s %-25s %-25s %-s\n",$1,$4,$5,$6}')
check6080=$(sudo /bin/netstat -anlutp|awk '$4~/:6080$/ {printf "%-6s %-25s %-25s %-s\n",$1,$4,$5,$6}')
check8008=$(sudo /bin/netstat -anlutp|awk '$4~/:8008$/ {printf "%-6s %-25s %-25s %-s\n",$1,$4,$5,$6}')
check15551=$(sudo /bin/netstat -anlutp|awk '$4~/:15551$/ {printf "%-6s %-25s %-25s %-s\n",$1,$4,$5,$6}')
conflict=0
dockerfile=Docker_Centos-7.6.1810-S
dockerfolder=centos-7.6/
serverimagefile=topav-V1.0.2.0.25
servercomposefile=topav-V1.0.2.0.25-compose
servercomposefolder=topav/
pathserverfile=linux*
pathserverfolder=/root/syspatch
function installedr(){
	        echo -e "--------开始安装--------"
		echo -e "step1 解压文件"
		echo -e "解压docker和docker-compose到当前目录"
		sudo tar -zxvf "$dockerfile".tar.gz
	        echo -e "解压镜像文件到当前目录"
                sudo tar -zxvf "$serverimagefile".tar.gz
		echo -e "解压编排文件到当前目录"
                sudo tar -zxvf "$servercomposefile".tar.gz
	        echo -e "解压补丁服务器文件到根目录"
                sudo tar -zxvf linux_syspath*.tar.gz -C /	       
		echo -e "step2.1 安装docker服务"
		sudo rpm -ivh "$dockerfolder"*.rpm --nodeps --force
		echo -e "step2.2 启动docker服务"
		sudo systemctl enable docker && systemctl start docker
		echo -e "step3.1 安装杀毒服务服务"
		sudo make -C "$servercomposefolder"  install
		echo -e "step3.2 加载服务端镜像文件"
		sudo docker load -i "$serverimagefile".tar
		echo -e "step3.3 启动服务"
		sudo topav up
		echo -e "step4.1 加载补丁服务器镜像文件"
		sudo docker load -i "$pathserverfolder".tar
		echo -e "step4.2 启动服务"
		sudo topsec up
		echo -e "---开始检查各服务状态---"
		echo -e "docker运行情况:"
		sudo systemctl status docker
		echo -e "docker相关信息:"
		sudo docker info
		echo -e "EDR管理中心服务端运行情况:"
		sudo topav ps
		echo -e "补丁服务器运行情况:"
                sudo topsec ps
		echo -e "-----添加防火墙配置-----"
		sudo iptables -F
		sudo firewall-cmd --zone=public --add-port=80/tcp --permanent
		sudo firewall-cmd --zone=public --add-port=443/tcp --permanent
		sudo firewall-cmd --zone=public --add-port=8090/tcp --permanent
		sudo firewall-cmd --zone=public --add-port=8004/tcp --permanent
		sudo firewall-cmd --zone=public --add-port=6080/tcp --permanent
		sudo firewall-cmd --zone=public --add-port=8008/tcp --permanent
		sudo firewall-cmd --zone=public --add-port=15551/tcp --permanent
		sudo firewall-cmd --reload
		echo -e "安装完成，请通过浏览器或者curl命令检查是否能正常打开web页面"
	}
if [ "$check80"x != ""x ];then
	echo -e "检查80(客户端下载)端口疑似被占用:"
	echo -e "$check80"
	conflict=1
fi
if [ "$check443"x != ""x ];then
	echo -e "检查443(web管理)端口疑似被占用:"
	echo -e "$check443"
	conflict=1
fi
if [ "$check8090"x != ""x ];then
	echo -e "检查8090(客户端与服务端通讯)端口疑似被占用:"
	echo -e "$check8090"
	conflict=1
fi
if [ "$check8004"x != ""x ];then
	echo -e "检查8004(TD与服务端通讯)端口疑似被占用:"
	echo -e "$check8004"
	conflict=1
fi
if [ "$check6080"x != ""x ];then
	echo -e "检查6080(服务端与NoVNC)端口疑似被占用:"
	echo -e "$check6080"
	conflict=1
fi
if [ "$check8008"x != ""x ];then
	echo -e "检8008(打标签工具)端口疑似被占用:"
	echo -e "$check8008"
	conflict=1
fi
if [ "$check15551"x != ""x ];then
	echo -e "检查15551(补丁服务器)端口疑似被占用:"
	echo -e "$check15551"
	conflict=1
fi
echo -e "内核版本:$corever"
echo -e "检查已结束"
if [ "$conflict" -eq 1 ];then
	read -p "疑似端口存在冲突情况，如果需要继续安装请输入y并回车确认:" confirmcode
	if [ "$confirmcode"x = yx ];then
		installedr
	else
		exit
	fi
else
	installedr
fi
